**<h1 style="color: #035aa6; font-size: 40px;">SHOP APP API</h1>**
<br>
<h2 style="color: #fcbf1e">Installation steps</h2>
After cloning the project from the Git server relocate to the project folder via terminal and follow the steps. 
```shell script
curl -sS https://getcomposer.org/installer | php
```
```shell script
php composer.phar install
```
```shell script
cd shop_app
```
```shell script
cp .env.bak .env
```
```shell script
./vendor/bin/sail up -d
```

<br>
<h2 style="color: #fcbf1e">Steps run the app</h2>
After project installation relocate to the project directory via terminal and execute the command.

```shell script
./vendor/bin/sail up -d
```
<h3>In order to stop it execute the command</h3>
```shell script
./vendor/bin/sail stop
```

<br>
<h2 style="color: #fcbf1e">Steps to remove the app</h2>
From the terminal relocate to the project directory.

```shell script
./vendor/bin/sail stop
```
```shell script
docker rm shop_app_laravel.test_1 shop_app_mailhog_1 shop_app_mysql_1 shop_app_redis_1
```
```shell script
docker rmi sail-8.0/app
```
```shell script
yes | docker image prune -a
```

